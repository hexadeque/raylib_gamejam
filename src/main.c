// Near the end of this project I lost intrest and just worked on it to have something to submit to the jam and in some places it shows

#include <stdio.h>
#include <math.h>
#include "raylib.h"
#include "vecmath.h"
#include <emscripten/emscripten.h>

#define ICEBLUE    (Color){ 216, 251, 255, 255 }
#define REALLYDARKGRAY   (Color){ 40, 40, 40, 255 }
#define SCREEN_WIDTH 900
#define SCREEN_HEIGHT 700
#define WALL_HEIGHT 1
#define BALL_RADIUS 0.5f
#define GOAL_RADIUS 0.75f
#define ROPE_LENGTH 2
#define COLLISION_RESOLUTION 20
#define MUD_START 5
#define ICE_START 9
#define LAST_LEVEL 13

static void Update(void);
static void ResetPlayer(void);
static void LoadLevel(int);
static Vector2 ToScreenSpace(Vector3);

static const Camera CAMERA = { { 0.0f, 20.0f, 0.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, -1.0f }, 14, CAMERA_ORTHOGRAPHIC };
// Wacky use of Vector3's is because this was gonna have 3D graphics before I gave up
static const Vector3 STARTING_POSES[] = {
    {-7, 0, -5},
    {-6.5, 0, 5},
    {-7, 0, -4.5},
    {-6.5, 0, 5},
    {-7, 0, -4.5},
    {-7, 0, 5},
    {-7, 0, -3.25},
    {-6.5, 0, -5},
    {-6, 0, -5},
    {7, 0, 5},
    {-6.5, 0, -5},
    {-6.5, 0, 5},
    {-4, 0, 3.5},
    {7, 0, -4.5}
};
static const Vector3 GOALS[] = {
    {7, 0, 5},
    {7, 0, -4.5},
    {-7, 0, 4.5},
    {6.5, 0, -5},
    {-7, 0, 4.5},
    {7, 0, -5},
    {-7, 0, 3.25},
    {6.5, 0, -5},
    {6, 0, 5},
    {-7, 0, -5},
    {7, 0, 4.5},
    {6.5, 0, 5},
    {-3.5, 0, 0},
    {-2.5, 0, 1}
};

static int level = 13;
static Model levelModel;
static Vector3 ballPos;
static Vector3 ballVel;
static bool alive = false;


int main(void)
{
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "raylib game template");
    LoadLevel(level);
    ResetPlayer();

    emscripten_set_main_loop(Update, 60, 1);

    UnloadModel(levelModel);

    return 0;
}

static void Update(void) {
    if (level == LAST_LEVEL) {
        BeginDrawing();
            ClearBackground(RAYWHITE);
            DrawText("A game with a\nBall", 50, 100, 100, DARKGRAY);
        EndDrawing();

        return;
    }

    Color ballColor = RED, wallColor = level < MUD_START ? GRAY : level < ICE_START ? REALLYDARKGRAY : SKYBLUE, floorColor = level < MUD_START ? LIGHTGRAY : level < ICE_START ? DARKBROWN : ICEBLUE;
    Vector3 mousePos = (Vector3) { (GetMouseX() - 450) / 50.0f, 0, (GetMouseY() - 350) / 50.0f };
    Vector2 relativePos;

    if (alive) {
        Vector3 prevBallPos = ballPos;

        ballPos.x += ballVel.x;
        ballPos.z += ballVel.z;

        Vector3 relativePos = ballPos;
        relativePos.x -= mousePos.x;
        relativePos.y = 0;
        relativePos.z -= mousePos.z;

        if (relativePos.x*relativePos.x + relativePos.z*relativePos.z > ROPE_LENGTH*ROPE_LENGTH) {
            float scale = ROPE_LENGTH / vec3len(relativePos);
            relativePos.x *= scale;
            relativePos.z *= scale;

            ballPos = relativePos;
            ballPos.x += mousePos.x;
            ballPos.z += mousePos.z;
        }

        float slipperyness = level < MUD_START ? 0.95f : level < ICE_START ? 0.975f : 1;

        ballVel.x = (ballPos.x - prevBallPos.x) * slipperyness;
        ballVel.z = (ballPos.z - prevBallPos.z) * slipperyness;

        for (float i = 0; i < 2*PI; i += 2*PI / COLLISION_RESOLUTION) {
            Vector3 direction = { cos(i), 0, sin(i) };
            Ray ray = { ballPos, direction };
            RayCollision collision = GetRayCollisionModel(ray, levelModel);
            if (collision.hit && collision.distance <= BALL_RADIUS)
                ResetPlayer();
        }

        Vector3 relativeGoalPos = ballPos;
        relativeGoalPos.x -= GOALS[level].x;
        relativeGoalPos.z -= GOALS[level].z;
        if (relativeGoalPos.x*relativeGoalPos.x + relativeGoalPos.z*relativeGoalPos.z <= GOAL_RADIUS*GOAL_RADIUS) {
            LoadLevel(++level);
            ResetPlayer();
        }

    } else {
        ballColor = WHITE;
        wallColor = LIGHTGRAY;
        floorColor = DARKGRAY;
        
        relativePos = (Vector2) { ballPos.x - mousePos.x, ballPos.z - mousePos.z };
        if (relativePos.x*relativePos.x + relativePos.y*relativePos.y <= BALL_RADIUS*BALL_RADIUS)
            alive = true;
    }

    BeginDrawing();
        ClearBackground(floorColor);

        BeginMode3D(CAMERA);
            DrawModel(levelModel, (Vector3) {0, WALL_HEIGHT, 0}, 1.0f, wallColor);
        EndMode3D();

        if (alive) {
            DrawCircleV(ToScreenSpace(GOALS[level]), GOAL_RADIUS * 50, GREEN);
            DrawLineEx(ToScreenSpace(ballPos), ToScreenSpace(mousePos), 5, BROWN);
        }

        DrawCircleV(ToScreenSpace(ballPos), BALL_RADIUS*50, ballColor);

        if (level == 0 && !alive)
            DrawText("Hover over the circle to start.", 50, 654, 30, DARKGRAY);

    EndDrawing();
}

static void ResetPlayer(void) {
    alive = false;
    ballPos = STARTING_POSES[level];
    ballVel = (Vector3) {0, 0, 0};
}

static void LoadLevel(int level) {
    UnloadModel(levelModel);
    char fileName[22];
    sprintf(fileName, "resources/level%d.obj", level);
    levelModel = LoadModel(fileName);
}

static Vector2 ToScreenSpace(Vector3 v) {
    return (Vector2) { v.x * 50 + 450, v.z * 50 + 350 };
}
