#include "vecmath.h"
#include <math.h>

float vec3len(Vector3 v) {
    return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}
